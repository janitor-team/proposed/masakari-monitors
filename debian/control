Source: masakari-monitors
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 po-debconf,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx <!nodoc>,
Build-Depends-Indep:
 python3-automaton,
 python3-consul,
 python3-coverage,
 python3-ddt,
 python3-hacking,
 python3-keystoneauth1,
 python3-libvirt,
 python3-lxml,
 python3-openstackdocstheme,
 python3-openstacksdk,
 python3-os-testr,
 python3-oslo.cache,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.middleware,
 python3-oslo.privsep,
 python3-oslo.service,
 python3-oslo.utils,
 python3-oslotest,
 python3-pycodestyle,
 python3-testscenarios,
 python3-testtools,
 python3-stestr,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/masakari-monitors
Vcs-Git: https://salsa.debian.org/openstack-team/services/masakari-monitors.git
Homepage: https://opendev.org/openstack/masakari-monitors

Package: masakari-instance-monitor
Architecture: all
Depends:
 masakari-monitors-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack Virtual Machine High Availability (VMHA) - instance monitor
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 Monitors for Masakari provides Virtual Machine High Availability (VMHA)
 service for OpenStack clouds by automatically detecting the failure events
 such as VM process down, provisioning process down, and nova-compute host
 failure. If it detect the events, it sends notifications to the masakari-api.
 .
 This package contains the Masakari instance monitor.

Package: masakari-host-monitor
Architecture: all
Depends:
 masakari-monitors-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack Virtual Machine High Availability (VMHA) - host monitor
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 Monitors for Masakari provides Virtual Machine High Availability (VMHA)
 service for OpenStack clouds by automatically detecting the failure events
 such as VM process down, provisioning process down, and nova-compute host
 failure. If it detect the events, it sends notifications to the masakari-api.
 .
 This package contains the Masakari host monitor.

Package: masakari-introspective-instance-monitor
Architecture: all
Depends:
 masakari-monitors-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack VirtualMachine High Availability (VMHA) - introspective host monitor
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 Monitors for Masakari provides Virtual Machine High Availability (VMHA)
 service for OpenStack clouds by automatically detecting the failure events
 such as VM process down, provisioning process down, and nova-compute host
 failure. If it detect the events, it sends notifications to the masakari-api.
 .
 This package contains the Masakari introspective instance monitor.

Package: masakari-monitors-common
Architecture: all
Depends:
 adduser,
 pacemaker-cli-utils,
 python3-masakari-monitors (= ${binary:Version}),
 tcpdump,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Virtual Machine High Availability (VMHA) - monitors common files
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 This package contains common files for Masakari monitors.

Package: masakari-monitors-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack Virtual Machine High Availability (VMHA) - doc
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 This package contains the monitors documentation.

Package: masakari-process-monitor
Architecture: all
Depends:
 masakari-monitors-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack VM High Availability (VMHA) - process monitor
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 Monitors for Masakari provides Virtual Machine High Availability (VMHA)
 service for OpenStack clouds by automatically detecting the failure events
 such as VM process down, provisioning process down, and nova-compute host
 failure. If it detect the events, it sends notifications to the masakari-api.
 .
 This package contains the Masakari process monitor.

Package: python3-masakari-monitors
Architecture: all
Section: python
Depends:
 python3-automaton,
 python3-consul,
 python3-keystoneauth1,
 python3-libvirt,
 python3-lxml,
 python3-openstacksdk,
 python3-oslo.cache,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.middleware,
 python3-oslo.privsep,
 python3-oslo.service,
 python3-oslo.utils,
 python3-pbr,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Virtual Machine High Availability (VMHA) - Python files
 Masakari provides Virtual Machine High Availability (VMHA) service for
 OpenStack clouds by automatically recovering the KVM-based Virtual
 Machine(VM)s from failure events such as VM process down, provisioning
 process down, and nova-compute host failure. It also provides API service for
 manage and control the automated rescue mechanism.
 .
 Monitors for Masakari provides Virtual Machine High Availability (VMHA)
 service for OpenStack clouds by automatically detecting the failure events
 such as VM process down, provisioning process down, and nova-compute host
 failure. If it detect the events, it sends notifications to the masakari-api.
 .
 This package contains the Python files and libraries for Masakari monitors.
